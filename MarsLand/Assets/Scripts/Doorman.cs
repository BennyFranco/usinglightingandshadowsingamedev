﻿using UnityEngine;
using System.Collections;

public class Doorman : MonoBehaviour {

	public float welcome_speed_factor;
	public Transform gate_left;
	public Transform gate_right;
	public Light[] entry_light = new Light[2];

	void Awake()
	{
		foreach (Light light in entry_light)
			light.intensity = 0;
	}

	void OnTriggerEnter (Collider collider)
	{
		if (collider.name=="hero")
		{
			StopAllCoroutines();
			StartCoroutine(GateAccess(true));
		}
	}

	
	void OnTriggerExit (Collider collider)
	{
		if (collider.name=="hero")
		{
			StopAllCoroutines();
			StartCoroutine(GateAccess(false));
		}
	}


	IEnumerator GateAccess(bool direction)
	{
		Vector3 old_pos_gate_left = gate_left.localPosition;
		Vector3 old_pos_gate_right = gate_right.localPosition;

		if (direction)
		{
			float i = 0;

			while (i < 1)
			{
				gate_left.localPosition = Vector3.Lerp(old_pos_gate_left, new Vector3(1.74f, old_pos_gate_left.y, old_pos_gate_left.z), i);
				gate_right.localPosition = Vector3.Lerp(old_pos_gate_right, new Vector3(-1.74f, old_pos_gate_right.y, old_pos_gate_right.z), i);
				foreach (Light light in entry_light)
					light.intensity = Mathf.Lerp (light.intensity, 15, i);
				i += Time.deltaTime * welcome_speed_factor;
				yield return null;
			}
		} else {
			float i = 0;
			while (i < 1)
			{
			gate_left.localPosition = Vector3.Lerp(old_pos_gate_left, new Vector3(0, old_pos_gate_left.y, old_pos_gate_left.z), i);
			gate_right.localPosition = Vector3.Lerp(old_pos_gate_right, new Vector3(0, old_pos_gate_right.y, old_pos_gate_right.z), i);
			foreach (Light light in entry_light)
				light.intensity = Mathf.Lerp (light.intensity, 0, i);
				i += Time.deltaTime * welcome_speed_factor;
			yield return null;
			}
		}

		yield return null;

	}
}
