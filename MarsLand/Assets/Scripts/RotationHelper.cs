﻿using UnityEngine;
using System.Collections;

public class RotationHelper : MonoBehaviour {

	public Vector3 rot_dirs;
	
	
	void Update()
	{
		transform.Rotate(rot_dirs * Time.deltaTime);
	}
	
	
}
