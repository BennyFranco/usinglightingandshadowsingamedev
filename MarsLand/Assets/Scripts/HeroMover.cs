﻿using UnityEngine;
using System.Collections;

public class HeroMover : MonoBehaviour {

	public float motion_speed;
	public float rotation_speed;
	

	void Start () {
		
	}
	
	void Move()
	{
		float delta_speed = Time.deltaTime * motion_speed;
		
		if (Input.GetKey(KeyCode.UpArrow))
		{
			transform.Translate(Vector3.forward * -delta_speed);
		}
		
		if (Input.GetKey(KeyCode.DownArrow))
		{
			transform.Translate(Vector3.forward * delta_speed);
		}
		
	}
	
	void Turn()
	{
		float delta_rotation_speed = Time.deltaTime * rotation_speed;
		
		if (Input.GetKey(KeyCode.RightArrow))
		{
			transform.Rotate(Vector3.up * delta_rotation_speed );
		}
		
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			transform.Rotate(Vector3.up * -delta_rotation_speed);
		}
	}

	void Update () 
	{
		Move ();
		Turn ();
	}
}
